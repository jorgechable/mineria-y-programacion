package practicamineria;

import java.io.FileWriter; //para escribir txt
import java.io.IOException; //entrada y salida
import java.io.FileNotFoundException; //archivo no encontrado


public class NumerosAleatorios {

    public static void main(String[] args) throws IOException {

        FileWriter fichero = new FileWriter("C:/Users/De la cruz/Documents/ISMA-8 LINDSAY/mineria/linsi.txt"); //Crea un fichero en la ruta.
        int aleatorio = (int) (Math.random() * 100); //desde donde va autogenerarse                            //Numero aleatorio entre 1 y 100
        // int aleatorio = 7;
        int numero = 0;                                                                                        //Variable numero
        System.out.println(aleatorio);                            //Se imprime el numero que se creo aleatoriamente 
        for (int x = 0; x < 100; x++) {                                           //Donde se generan hasta tener 100 numeros
            fichero.write("" + numero + "" + '\n');                               //Se escribe en el TXT 
            numero += aleatorio;                                                  //

        }
        fichero.close();
    }

}
