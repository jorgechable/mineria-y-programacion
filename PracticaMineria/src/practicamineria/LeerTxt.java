package practicamineria;

import java.io.*;

public class LeerTxt {

    public static void muestraContenido(String archivo) throws FileNotFoundException, IOException {

        String cadena;                                          //Crear variable llamada cadena
        FileReader f = new FileReader(archivo);                 //Lee archivo y guarda en f 
        BufferedReader b1 = new BufferedReader(f);              //Lee el texto
        FileReader f2 = new FileReader(archivo);
        BufferedReader c = new BufferedReader(f2);
        int i = 0;
        int cantidad = 0;
        int ultimo = 0;                                       //Variable cantidad que guardara cuantos numeros son
        while ((cadena = b1.readLine()) != null) {              //Lee linea por linea el archivo, esto lo hace con while hasta que se acaben los numeros.
            cantidad++;                                         //Y guarda cuantos numeros son en cantidad
        }
        System.out.println("Cuantos numeros son:" + cantidad);  //Imprime cuantos numeros son
        b1.close();                                             //Se cierra 
        int aa[] = new int[cantidad];                           //Crea arreglo con el tamaño de cantidad
        while ((cadena = c.readLine()) != null) {               //Lee linea por linea
            aa[i] = Integer.parseInt(cadena);                       //Se parsea
            System.out.println(aa[i]);                          //Se imprime el arreglo 
            i++;                                                //Y la variable i se incrementara
        }
        int diferencias[] = new int[cantidad - 1];
        for (int x = 0; x < (cantidad - 1); x++) {             //Para determinar el patron se selecciona el ultimo de 
            diferencias[x] = aa[x + 1] - aa[x];
        }
        c.close();

        int maximoNumRepeticiones = 0;  // moda
        int moda = 0;

        for (int ii = 0; ii < diferencias.length; ii++) {
            int numRepeticiones = 0;
            for (int j = 0; j < diferencias.length; j++) {
                if (diferencias[ii] == diferencias[j]) {
                    numRepeticiones++;
                }
                if (numRepeticiones > maximoNumRepeticiones) {
                    moda = diferencias[ii];
                    maximoNumRepeticiones = numRepeticiones;
                }
            }
        }
        System.out.println("El patron es de: " + moda);

        int errores = 0;
        for (int yy = 0; yy < diferencias.length; yy++) {
            if (diferencias[yy] != moda) {
                errores++;

            }

        }
        errores = errores / 2;
        if (errores == 1) {
            System.out.println("Existe " + errores + " error");
        }
        if (errores > 1) {
            System.out.println("Existen " + errores + " errores");
        }
    }

    public static void main(String[] args) throws IOException {
        muestraContenido("C:/Users/De la cruz/Documents/ISMA-8 LINDSAY/mineria/linsi.txt");
    }
}
