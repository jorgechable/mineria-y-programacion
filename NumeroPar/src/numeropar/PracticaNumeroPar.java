/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numeropar;
import java.util.Scanner;

/**
 *
 * @author De la cruz
 */
public class PracticaNumeroPar {

    public static void main(String[] args) {

        int num1, num2, resultado;
        Scanner sc = new Scanner(System.in); //Se crea el lector

        System.out.println("digite el primer numero");
        num1 = sc.nextInt(); //Se guarda el primer numero directamente con nextInt()

        System.out.println("digite el segundo numero");
        num2 = sc.nextInt();

        resultado = num1 + num2; // Almacenamos la suma en la variable resultado

        System.out.println("La suma de " + num1 + " + " + num2 + " es : " + resultado);

        if (resultado % 2 == 0) {
            System.err.println("El número " + resultado + " Es par");
        } else {
            System.err.println("El número " + resultado + " Es impar");
        }
    }

}
