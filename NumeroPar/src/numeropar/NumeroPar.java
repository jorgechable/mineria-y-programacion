package NumeroPar;
import java.io.*;
public class NumeroPar {
    public static void muestraContenido(String archivo) throws FileNotFoundException, IOException {
        String cadena;                                    //creo cadena
        FileReader f = new FileReader(archivo);           //lee un archivo y lo guarda en f
        BufferedReader b1 = new BufferedReader(f);        //Lee un texto de f
        FileReader f2 = new FileReader(archivo);
        BufferedReader b2 = new BufferedReader(f2);
        int i = 0;                                  //crea variables
        int par = 0;
        int impar = 0;
        int cantidad = 0;
        char tabular = 9;                                               //creo tabular
        while ((cadena = b1.readLine()) != null) {                      //de las variables lee linea por linea en txt
            cantidad++;                                                 //se guarda en cantidad y cuantos son
        }
        System.out.println("Son: " + cantidad + " numeros");           //cuantos numeros son
        b1.close();
        int aa[] = new int[cantidad];                                   //Crea arreglo []    //cantidad,tamaño
        int imprimir=0;                                                 //crea arreglo para el tabulador
        while ((cadena = b2.readLine()) != null) {
            aa[i] = Integer.parseInt(cadena); 
            if(imprimir<19){                                            //Si imprimir es menor que 19 
                System.out.print(aa[i]+" "+tabular);                    //el arreglo imprime  aa[i] y se pone el tabular
                imprimir++;                                             //se imprime    
            }else{
                System.out.println(aa[i]);
                imprimir=0;
            }
            if(aa[i] % 2 == 0) {                                     //agarra el arreglo y hacer la division para que nos de el residuo 0
                par++;                                               //guarda la cantidad de pares que hay
                    // System.out.println("El numero " + aa[i] + " es par.");           //imprime si es par
            } else {
                   impar++;                                             //guarda la cantidad de impares que hay
                    // System.out.println("El numero " + aa[i] + " es impar.");       //imprime si es impar
            }
            i++;                                                            //y se va aumentando
        }
        System.out.println("");
        System.out.println(par + " numeros son pares");      //imprime la cantidad de pares
        System.out.println(impar + " numeros son impares");  //imprime la cantidad de impares
        b2.close();
    }
    public static void main(String[] args) throws IOException {
        muestraContenido("C:/Users/De la cruz/Documents/ISMA-8 LINDSAY/mineria/linsi.txt");
    }
}
